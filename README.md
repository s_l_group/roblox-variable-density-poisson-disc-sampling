# Variable Density Poisson Disc Sampling

![Alt text](spiral_distribution.png)

## Description
This Roblox project uses a variable density poisson disc sampling algorithm to produce randomly placed points on a grid with decreasing density.

Play it on <a href="https://www.roblox.com/games/17038572487/Adaptive-Point-Distribution">Roblox</a>!
